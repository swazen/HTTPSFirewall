# University of Lorraine,
# CNRS, Nancy
# Wazen Shbair
# shbair.wazen@loria.fr
# wazen.shbair@gmail.com
# October, 2016
# ---------------------------
# Requirements : iptables, numpy, dpkt,ipadress, sckilearn
# Running: Model folder includes classification model, run the script using sudo command
# ---------------------------

#!/usr/bin/python

import subprocess
import os
import sys
import hashlib
p = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'third_party', 'dpkt')
if p not in sys.path:
    sys.path.insert(0, p)
import sys
import socket
import nfqueue
import dpkt
import shlex
from socket import AF_INET, AF_INET6, inet_ntoa
from dpkt import ip, tcp, hexdump, ssl,ethernet
sys.path.append('python')
sys.path.append('build/python')
sys.path.append('dpkt-1.8')
from tld import get_tld
from functions import *
from collections import defaultdict
from multiprocessing import Process
import ipaddress as ipaddr
import numpy as np
from sklearn import tree
import tldextract
import resource

####################################################
# Flow Identification Class
####################################################
class Packet:
    ipsrc=""
    sport=""
    ipdst=None
    dport=""
    hashID=""
    SYN=False
    ACK=False
    timestamp=""
    APPData=False
    Handshake=False
    length=0
    sni=""
    SYNEND=False
    type=0
    payload=None
    APPlength=0

    def __init__(self, ipsrc,sport,ipdst,dport,SYN, ACK, timestamp, APP, HS, length, sni,snyend, type,payload,APPlength):
        self.ipsrc = ipsrc
        self.sport=sport
        self.ipdst=ipdst
        self.dport=dport
        temp = hashlib.sha1(str(int(socket.inet_aton(self.ipsrc).encode('hex'),16) + self.sport + int(socket.inet_aton(self.ipdst).encode('hex'),16) + self.dport))
        self.hashID = temp.hexdigest()
        self.SYN=SYN
        self.ACK=ACK
        self.timestamp=timestamp
        self.APPData=APP
        self.Handshake=HS
        self.length=length
        self.sni=sni
        self.SYNEND=snyend
        self.type=type
        self.payload=payload
        self.APPlength=APPlength

    def compare(self, ipsrc,sport,ipdst,dport):
        if self.ipsrc==int(socket.inet_aton(ipsrc).encode('hex'),16) and self.sport==sport and self.ipdst==int(socket.inet_aton(ipdst).encode('hex'),16) and self.dport==dport:
            return True
        elif self.ipsrc==int(socket.inet_aton(ipdst).encode('hex'),16) and self.sport==dport and self.ipdst==int(socket.inet_aton(ipsrc).encode('hex'),16) and self.dport==sport:
            return True
        else:
            return False

    def PrintPacket(self):
        x= self.sni +","+ str(self.length)+","+ str(self.Handshake)+","+str(self.APPData)+","+str(self.timestamp) +","+str(self.ipsrc)+","+str(self.ipdst)+","+str(self.sport)+"."+str(self.dport)
        return x

####################################################
# IP comparison
####################################################
def comapreIpaddress32(ip1,ip2):
    IP1 = ipaddr.ip_address(ip1)
    IP2 = ipaddr.ip_address(ip2)
    if IP1 == IP2:
        return True
    else:
        return False
####################################################
# Main function
####################################################
def main(arvg):

    filename="training-dataset.csv"
    p = subprocess.Popen(
        ["iptables", "-I", "INPUT", "-p", "tcp", "-m", "state", "--state", "ESTABLISHED,RELATED,NEW", "-j", "NFQUEUE",
         "--queue-num", "0"],
        stdout=subprocess.PIPE)
    output, err = p.communicate()
    p2 = subprocess.Popen(
        ["iptables", "-I", "OUTPUT", "-p", "tcp", "-m", "state", "--state", "ESTABLISHED,RELATED,NEW", "-j", "NFQUEUE",
         "--queue-num", "0"],
        stdout=subprocess.PIPE)
    p2.communicate()
    # Input/output queue
    print "| Real-Time HTTPS Monitoring Firewall |"
    print "  -----------------------------------  "	
    print "[+] IPtables Rules Installed Sucssefully"
    print "[+] Setting callback functions for input/output queue"
    q2 = nfqueue.queue()
    q2.open()  # 0 is the number of Queue
    q2.set_callback(cb)  # input queue
    q2.create_queue(0)
    q2.set_queue_maxlen(307200)

    global flowscounter
    flowscounter=[]

    global FCRT
    FCRT = defaultdict(list)

    global stopcollect
    stopcollect = []

    global saved
    saved = []

    global model
    #model= joblib.load("Models/firstlevel.pkl")
    model= RealtimeClassifier(filename)

    global BCRT
    BCRT=defaultdict(list)
    global ACC
    ACC=defaultdict(list)
    global RIdent
    RIdent=[]
    global ALLlength
    global TargetList
    global timer
    timer =0

    global Keyslist
    Keyslist=[]
    #TargetList=[line.strip() for line in open("targetlist.txt", 'r')]
    t1 = datetime.datetime.now().microsecond
    global blocking
    blocking=[]

    try:
        while True:
            q2.try_run()
    except KeyboardInterrupt:
        print "[+] Removing Rules from IPtables"
        t2= datetime.datetime.now().microsecond

        p2 = subprocess.Popen(["iptables","-F"], stdout=subprocess.PIPE)
        p2.communicate()


        falseresult=[]
        if len(ACC)!=0:
            for k in ACC:
                values=ACC[k]
                T = 0
                for line in values:
                    if SNIModificationbyone(line[0])==SNIModificationbyone(line[1]):
                        T=1
                if T==1:
                    RIdent.append(1)
                else:
                    RIdent.append(0)
                    falseresult.append(line)
            print "[+] Result: "+str(RIdent.count(1)*1.0/len(RIdent))
            print "[+] Number of flows: "+str(len(RIdent))
            print "[+] Flows Counter= "+str(len(flowscounter))
            print "[+] Time="+str(t2-t1)

###########################################################################
# Callback function NFqueue Queue input/output
###########################################################################
def cb(i, payload):
    delaytime =open("delaytime.csv",'a')
    data = payload.get_data()
    pkt = ip.IP(data)
    sport = int(pkt.tcp.sport)
    dport = int(pkt.tcp.dport)
    key=""
    if sport==443 or dport==443:
        Info = ParsPackets(payload,(datetime.datetime.now().microsecond))
        if Info.SYN: #and Info.hashID not in stopcollect:
            R=GetBCRTRecord(Info.hashID)
            if not R and not Info.ACK:
                InsertBCRTRecord(Info) # New Syn appear
                #print "New SYN appear ID: "+Info.hashID
            elif R:
                if Info.ACK:
                   UpdateRecord(Info.hashID)
                   InsertFCRTRecord(Info)
        else:
            valid = GetFCRTRecord(Info.hashID)
            if valid and Info.hashID not in stopcollect and (Info.Handshake or Info.APPData) and Info.hashID not in blocking:
               InsertFCRTRecord(Info)
               if len(FCRT[Info.hashID])>=7: #5 #and Info.APPData:
                    r=StartIdenification(Info.hashID)
        if Info.hashID not in blocking:
            payload.set_verdict(nfqueue.NF_ACCEPT)
        else:
            payload.set_verdict(nfqueue.NF_DROP)
            #print "[-] Invalid Connection detected"
            p2 = subprocess.Popen(
                ["iptables", "-I", "INPUT", "-s", Info.ipdst, "-j", "DROP"], stdout=subprocess.PIPE)
            p2.communicate()

            p3 = subprocess.Popen(
                ["iptables", "-I", "OUTPUT", "-d", Info.ipdst, "-j", "DROP"], stdout=subprocess.PIPE)
            p3.communicate()

    else:
        payload.set_verdict(nfqueue.NF_ACCEPT)
##########################################################################
# Pars Packets
#########################################################################
def ParsPackets(payload,t):
    try:
        data = payload.get_data()
        pkt = ip.IP(data)
        ipsrc = inet_ntoa(pkt.src)
        sport = pkt.tcp.sport
        ipdst = inet_ntoa(pkt.dst)
        dport = pkt.tcp.dport
        SYN=(pkt.tcp.flags & dpkt.tcp.TH_SYN)
        ACK=(pkt.tcp.flags & dpkt.tcp.TH_ACK)
        SYNEND=(pkt.tcp.flags & dpkt.tcp.TH_FIN)
        type1=0
        APP= (len(pkt.tcp.data)>0 and int(ord(pkt.tcp.data[0])) == 23)
        HS= (len(pkt.tcp.data)>0 and (int(ord(pkt.tcp.data[0])) == 22 or int(ord(pkt.tcp.data[0])) == 20) and Ishandshake(pkt))
        length= pkt.len
        APPlength=len(pkt.tcp.data)
        sni=""
        if int(pkt.tcp.dport) == 443 and len(pkt.tcp.data) > 0 and int(ord(pkt.tcp.data[0])) == 22:
            sni=GetSNI(pkt)
            #length=length-19 #remove the length of SNI
        pak = Packet(ipsrc,sport,ipdst,dport,SYN,ACK,t,APP, HS, length, sni,SYNEND,type1, pkt,APPlength)
        return pak
    except:
        pass

# ##########################################################################
# # SNI packet
# #########################################################################
def GetSNI(pkt):
    sni = ""
    try:
        if int(pkt.tcp.dport) == 443 and len(pkt.tcp.data) > 0 and int(ord(pkt.tcp.data[0])) == 22:
            records, bytes_used = dpkt.ssl.TLSMultiFactory(pkt.tcp.data)
            if len(records) > 0:
                for record in records:
                    if int(record.type) == 22 and len(record.data) > 0 and int(ord(record.data[0])) == 1:
                        handshake = dpkt.ssl.TLSHandshake(record.data)
                    if isinstance(handshake.data, dpkt.ssl.TLSClientHello):
                        client = dpkt.ssl.TLSClientHello(str(handshake.data))
                        # print hexdump(str(client.extensions))
                        for ext in client.extensions:
                            if int(ext.value) == 0:
                                sni = ext.data[5:]
                                break
    except:
        pass
    return sni
##########################################################################
# Identification Engine
#########################################################################
def StartIdenification(key):
    Ide=False
    if key not in saved:
        packets = FCRT[key]
        if len(packets)>=3:
            file2 = []
            file3 = []
            file4 = []
            file5 = []
            file6 = []
            file7 = []
            APPfile5=[]
            APPfile6=[]
            firstpacket=packets[0]
            sni = ""
            clienthellofeatures=[]
            serverhellofeatures=[]

            ipsrc = firstpacket.ipsrc
            destination=""
            for pkt in packets:
                if pkt.sni!="":
                    sni=pkt.sni
                if len(clienthellofeatures)==0:
                    clienthellofeatures = ClientHello(pkt.payload)
                if len(serverhellofeatures) == 0:
                    serverhellofeatures = ServerHelloFeatures(pkt.payload)
                if pkt.length > 0 and pkt.Handshake:
                    file2.append(pkt.length)
                    file3.append(pkt.timestamp)
                    if comapreIpaddress32(pkt.ipsrc,ipsrc):
                        file4.append(pkt.length)
                        file5.append(pkt.timestamp)
                    elif comapreIpaddress32(pkt.ipdst,ipsrc):
                        file6.append(pkt.length)
                        file7.append(pkt.timestamp)

                if pkt.APPData:
                   if comapreIpaddress32(pkt.ipsrc, ipsrc):
                       APPfile5.append(pkt.APPlength)
                   elif comapreIpaddress32(pkt.ipdst, ipsrc):
                       APPfile6.append(pkt.APPlength)

            #Features Collection
            Handshakefeatures=GetFeatures2(file2, file3, file4, file5, file6, file7)+ clienthellofeatures[:3]+ \
                              serverhellofeatures+AppdataStatistics(APPfile5)+ AppdataStatistics(APPfile6)
            NumberofApp= len(APPfile5)+len(APPfile6)
            total = len(APPfile5) + len(APPfile6) + len(file2)
            Handshakefeatures.append(NumberofApp)
            Handshakefeatures.append(total)
            Handshakefeatures.append(sni)
            flowscounter.append("1")

            #Prediction
            if len(Handshakefeatures)>48 and len(sni)>0:
                #if checkSNI(sni, clienthellofeatures[3]):  # Check SNI against fake SNI
                    Ide=True
                    resu=model.predict(Handshakefeatures[0:49])[0]
                    store=[resu,sni]
                    #print " "+ resu +" | "+ sni +" | (INVALID)"
                    flowscounter.append("1")
                    ACC[key].append(store)
                    # if resu in TargetList:
                    #    blocking.append(key)
                    #    stopcollect.append(key)
            del file2
            del file3
            del file4
            del file5
            del file6
            del file7
            del APPfile5
            del APPfile6
        return Ide
            #saved.append(key)
            #del FCRT[key]
##########################################################################
# Handshake Information
###########################################################################
def ClientHello(packets):
    features = []
    if len(packets.tcp.data) > 0 and int(ord(packets.tcp.data[0])) == 22:
        records, bytes_used = dpkt.ssl.TLSMultiFactory(packets.tcp.data)
        if len(records) > 0:
            for record in records:
                if record.type == 22 and len(record.data) > 0 and ord(record.data[0]) == 1:
                    handshake = dpkt.ssl.TLSHandshake(record.data)
                    # Client Hello
                    if isinstance(handshake.data, dpkt.ssl.TLSClientHello):
                        client = dpkt.ssl.TLSClientHello(str(handshake.data))
                        features.append(len(client.session_id))
                        features.append(client.num_ciphersuites)
                        features.append(len(client.extensions))
                        features.append(inet_ntoa(packets.dst))
    return features
#############################################
# Extract Features From Server Hello Packet
#############################################
def ServerHelloFeatures(packets):
    features = []
    if int(packets.tcp.sport) == 443 and len(packets.tcp.data) > 0 and int(ord(packets.tcp.data[0])) == 22:
        records, bytes_used = dpkt.ssl.TLSMultiFactory(packets.tcp.data)
        if len(records) > 0:
            for record in records:
                if record.type == 22 and len(record.data) > 0 and ord(record.data[0]) == 2:
                    handshake = dpkt.ssl.TLSHandshake(record.data)
                    # Server Hello
                    if isinstance(handshake.data, dpkt.ssl.TLSServerHello):
                        server = dpkt.ssl.TLSServerHello(str(handshake.data))
                        features.append(len(server.session_id))
                        features.append(server.cipher_suite)
                        features.append(len(server.extensions))
    return features
#################################################
# Extract Features From Application Data Packets
#################################################
def AppdataStatistics(pktsize):
    result=[]
    if len(pktsize) == 0:
        pktsize.append(0)
    result.append(math.ceil(numpy.mean(pktsize)))
    result.append(math.ceil(numpy.percentile(pktsize, 25)))
    result.append(math.ceil(numpy.percentile(pktsize, 50)))
    result.append(math.ceil(numpy.percentile(pktsize, 75)))
    result.append(math.ceil(numpy.var(pktsize)))
    result.append(math.ceil(numpy.max(pktsize)))
    del pktsize
    return result
#############################################
# Function for packet size statistics
#############################################
def PacketSizeCalculation(pktsize):
    result=[]
    if len(pktsize) == 0:
        pktsize.append(0)
    result.append(len(pktsize))
    result.append(math.ceil(numpy.mean(pktsize)))
    result.append(math.ceil(numpy.percentile(pktsize, 25)))
    result.append(math.ceil(numpy.percentile(pktsize, 50)))
    result.append(math.ceil(numpy.percentile(pktsize, 75)))
    result.append(math.ceil(numpy.var(pktsize)))
    result.append(math.ceil(numpy.max(pktsize)))
    return result
######################################################################
# Function for Calculating the Inter Arrival Time and make statistics
######################################################################
def IATCalculcation(arrival):
    arrivaltime= sorted (arrival)
    if len(arrivaltime)==0:
        arrivaltime.append(0)
    counter = 1
    IAT = []
    result=[]
    for item in arrivaltime:
        if (counter != len(arrivaltime)):
            IAT.append((arrivaltime[counter] - arrivaltime[counter - 1])/1000.0)
            counter = counter + 1
    if len(IAT)!=0:
        result.append(round(numpy.percentile(IAT, 25),2))
        result.append(round(numpy.percentile(IAT, 50),2))
        result.append(round(numpy.percentile(IAT, 75),2))
    else:
        result.append(round(numpy.percentile(arrivaltime, 25), 2))
        result.append(round(numpy.percentile(arrivaltime, 50), 2))
        result.append(round(numpy.percentile(arrivaltime, 75), 2))
    del IAT
    return result

# Function used to store statistics
def GetFeatures2(TCS_PSIZE, TCS_TIME, CS_PSIZE, CS_TIME, SC_PSIZE, SC_TIME):
    features = PacketSizeCalculation(TCS_PSIZE) + IATCalculcation(TCS_TIME) + PacketSizeCalculation(CS_PSIZE) + \
               IATCalculcation(CS_TIME) + PacketSizeCalculation(SC_PSIZE) + IATCalculcation(SC_TIME)
    return features

#############################################
# Check Application data & Handshake packets
#############################################
def IsApplicationDatapackets(packets):
    result=False
    if len(packets.tcp.data) > 0 and int(ord(packets.tcp.data[0])) == 23 \
            and (int(packets.tcp.dport) == 443 or int(packets.tcp.sport) == 443):
            records, bytes_used = dpkt.ssl.TLSMultiFactory(packets.tcp.data)
            if len(records) > 0:
                for record in records:
                    if record.type == 23 and len(record.data) > 0:
                        result=True
    return result
###########################################################################
def Ishandshake(packets):
    result=False
    try:
        if len(packets.tcp.data) > 0 and (int(ord(packets.tcp.data[0])) == 22 or int(ord(packets.tcp.data[0])) == 20)\
                and (int(packets.tcp.dport) == 443 or int(packets.tcp.sport) == 443):
            records, bytes_used = dpkt.ssl.TLSMultiFactory(packets.tcp.data)
            if len(records) > 0:
                for record in records:
                    if (record.type == 22 or record.type == 20 ) and len(record.data) > 0:
                        result=True
    except:
        #print hexdump(str(packets))
        pass
    return result

###########################################################################
# TCP Flow Reassembly Functions
# BCRT : Budding Connection Record Table
# FCRT : Formal Connection Record Table
###########################################################################
# Get BCRT Record
###########################################################################
def GetBCRTRecord(ID):
    valid = False
    result=BCRT[ID]
    for pkt in result:
        if pkt.hashID==ID:
           valid= True   # It means the packet is valid
           break
    return valid
###########################################################################
# Insert BCRT Record
###########################################################################
def InsertBCRTRecord(Info):
    BCRT[Info.hashID].append(Info)
###########################################################################
# Update Record
###########################################################################
def UpdateRecord(ID):
    for pkt in BCRT[ID]:
        if pkt.hashID == ID:
           pkt.ACK=True
           break
###########################################################################
# Insert FCRT Record
###########################################################################
def InsertFCRTRecord(Info):
    FCRT[Info.hashID].append(Info)
    Keyslist.insert(0,Info.hashID)
###########################################################################
# Get FCRT Record
###########################################################################
def GetFCRTRecord(ID):
    valid=False
    if ID in Keyslist:
        valid = True
    return valid
###########################################################################
# Build C4.5 Classifier
###########################################################################
def RealtimeClassifier(filename):
    print "[+] Model Building"
    train = read_csv(filename)
    target = np.array([x[51] for x in train])
    train = np.array([x[0:49] for x in train])
    rf=tree.DecisionTreeClassifier()
    rf.fit(train,target)
    #output="Models/firstlevel.pkl"
    #joblib.dump(rf,output)
    print "[+] Model is Ready "
    print "[+] Identification engine is ready"
    print "[+] Timer is ON"
    print "----------------------------------"
    print " Service Name | SNI detected " 	
    return rf
###########################################################################
# Read csv File
###########################################################################
def read_csv(file_path, has_header=True):
    with open(file_path) as f:
        if has_header: f.readline()
        data = []
        for line in f:
            line = line.strip().split(",")
            if line[0]!="Tnum":
                data.append([x for x in line])
    return data
###########################################################################
# SNI Processing for Training and Test
###########################################################################
def SNIModificationbyone(sni):
    newsni=""
    temp = tldextract.extract(sni)
    x = re.sub("\d+", "", temp.subdomain)  # remove numbers
    x = re.sub("[-,.]", "", x)  #remove dashes
    if len(x) > 0:
        newsni = temp.domain + "." + temp.suffix  # reconstruct the sni
    else:
        newsni = temp.domain + "." + temp.suffix

    return newsni

def SNIModificationbyonefull(sni):
    newsni=""
    temp = tldextract.extract(sni)
    x = re.sub("\d+", "", temp.subdomain)  # remove numbers
    x = re.sub("[-,.]", "", x)  #remove dashes
    if len(x) > 0:
        newsni = x+"."+temp.domain + "." + temp.suffix  # reconstruct the sni
    else:
        newsni = temp.domain + "." + temp.suffix
    return newsni
###########################################################################

def comapreIpaddress24(ip1,ip2):
    tempip1=ip1.split('.')
    tempip2=ip2.split('.')
    if int(tempip1[0])==int(tempip2[0]) and int(tempip1[1])==int(tempip2[1]) and int(tempip1[2])==int(tempip2[2]):
        return True
    else:
        return False
def comapreIpaddress16(ip1,ip2):
    tempip1= ip1.split('.')
    tempip2= ip2.split('.')
    if int(tempip1[0])==int(tempip2[0]) and int(tempip1[1])==int(tempip2[1]):
       return True
    else:
       return False
##################################
# search ip in list
###################################
def searchIpinList (destipaddress, iplist, sni):
    for dnsip in iplist:
        if comapreIpaddress32(destipaddress, dnsip):
            return True
        if comapreIpaddress24(destipaddress, dnsip):
            return True
        if comapreIpaddress16(destipaddress, dnsip):
            return True
    return False

######################################################
# Send DNS Request
######################################################
def getDNSAnswers(domain):
    try:
        cmd= 'dig +short ' + domain
        proc= subprocess.Popen(shlex.split(cmd), stdout=subprocess.PIPE)
        out= proc.communicate()[0]
        return out
    except:
        return "None"

###########################################################################
# Compare two sni
###########################################################################
def compareSNi(sni1, dnsanswer):
    f1 = get_tld("https://" + sni1)
    if f1 in dnsanswer:
        return True
    else:
        return False
###########################################################################
# SNI Verification function based on DNS
###########################################################################
def checkSNI(sni, destipaddress):
    print "[+] Checking SNI value"
    if len(sni)>1:
        dnsanswer = getDNSAnswers(sni)  # Get the DNS answer based on SNI
        #extract the IP list from DNS
        iplist= re.findall(r'[0-9]+(?:\.[0-9]+){3}', dnsanswer)

        # Search in the IP list
        if searchIpinList(destipaddress, iplist, sni):
            #print "[+] Valid Connection for: " +sni
            return True
        else:
            print "[-] ALARM : Invalid Connection detected to : " +sni
            print "[-] Distination IP address :"+destipaddress
            print "[-] DNS response: "+dnsanswer
            return False
            exit()
    else:
        return True
#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

if __name__ == "__main__":
    main(sys.argv[1:])
