#!/usr/lib/python
# Author Wazen Shbair
####################################################

import pyshark
import sys
import math
from datetime import *
from array import *
import numpy as np
from collections import Counter
from subprocess import Popen
import datetime
import time
from os import listdir
from os.path import isfile, join
import os
import csv
from collections import defaultdict
import re
import tldextract
from functions import *
from numpy import genfromtxt
from sklearn.ensemble import RandomForestClassifier
from collections import defaultdict
import pickle
from sklearn import tree
from sklearn.externals import joblib
from sklearn import cross_validation
from sklearn.metrics import accuracy_score
from sklearn.feature_selection import SelectKBest
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.metrics import confusion_matrix
import pylab as pl
#import networkx as nx
#import matplotlib.pyplot as plt
import socket, struct
import numpy
from sklearn import tree
import webbrowser

#########################################################################
#Function for blocked Streams Removal and parepare statisictic for multi-level classification
#########################################################################
####################################
# Handsheke process
####################################
def SreamsRemoval(Streamsfile, outputfile, reso):
    filterstream = []
    head = "Tnum,Avsize,PsizeQ1,PsizeQ2,PsizeQ3,Var,Max,IATQ1,IATQ2,IATQ3,CSTnum,CSAvsize,CSPsizeQ1,CSPsizeQ2,CSPsizeQ3,CSVar," \
           "CSMax,CSIATQ1,CSIATQ2,CSIATQ3,SCTnum,SCAvsize,SCPsizeQ1,SCPsizeQ2,SCPsizeQ3,SCsize_var,SCMax,SCIATQ1,SCIATQ2,SCIATQ3," \
           "ClientSessionIDLength, ClientChiperNum,ExtensionNUM,ServeressionIDLength,ServerChiper,ServerExtensionNUM,CSAppMean,CSAPP_Q1," \
           "CSAPP_Q2,CSAPP_Q3,CSAPP_var,CSAPP_max,SCAppMean,SCAPP_Q1,SCAPP_Q2,SCAPP_Q3,SCAPP_var,SCAPP_max, Appnum, total, class,class2 \n"

    f1 = open(outputfile, 'a')
    f1.write(head)
    file1 = open(Streamsfile)
    reader1 = csv.reader(file1, delimiter=',')

    file2 = open(Streamsfile)
    reader2 = csv.reader(file2, delimiter=',')

    snitmp=[]
    # Alising sub-domain
    for row in reader1:
        snitmp.append(SNIModificationbyone(row[48]))


    #Get just sni with more than "reso" traces
    uniqueServices = np.unique(snitmp)

    for x in uniqueServices:
        c = snitmp.count(x)
        if c < int(reso) and x not in filterstream:
            filterstream.append(x)
    for row in reader2:
        if SNIModificationbyone(row[48]) not in filterstream and row[0]!="Tnum": # 38 52
            temp = tldextract.extract(row[48])  #This is to extract the root Domain
            newsni = temp.domain + "." + temp.suffix
            f1.write(",".join(row[:48])+","+",".join(row[49:])+","+SNIModificationbyone(row[48])+ "," + newsni+"\n")

    f1.close()
    print "Ce Fini, Merci"



#***********************************************************************************
#SNi modification for the sub-domain parts only
#***********************************************************************************
def SNIModificationbyone(sni):
    temp = tldextract.extract(sni)
    x = re.sub("\d+", "", temp.subdomain)  # remove numbers
    x = re.sub("[-,.]", "", x)  #remove dashes
    if len(x) > 0:
        newsni = x + "." + temp.domain + "." + temp.suffix  # reconstruct the sni
    else:
        newsni = temp.domain + "." + temp.suffix
    #print(x)


    return newsni
##########################################################
def read_csv(file_path, has_header=True):
    with open(file_path) as f:
        if has_header: f.readline()
        data = []
        for line in f:
            line = line.strip().split(",")
            data.append([x for x in line])
    return data

def write_csv(file_path, data):
    with open(file_path, "w") as f:
        for line in data: f.write(",".join(line) + "\n")


##########################################################################
# Create First Level Classifier
#########################################################################
def firstmodelsCreators(filename):
    print "Create First Level Model ...."
    train = read_csv(filename)
    target = np.array([x[43] for x in train])
    train = np.array([x[0:42] for x in train])
    rf = RandomForestClassifier(n_estimators=100, n_jobs=30)
    rf.fit(train, target)
    #print datetime.datetime.now()
    #ac=cross_validation.cross_val_score(rf,train, target, cv=10,n_jobs=1)
    #print datetime.datetime.now()
    #print ac
    output = "Models/firstlevel.pkl"
    joblib.dump(rf, output)


#########################################################################
 
 
###################################
# Create Models by train set
####################################
def Lv2modelsCreators(trainset, keyset, flimit):
    #print "[+] Create Second level Models"
    streamdict = defaultdict(list)
    models = defaultdict(list)
    dataset = zip(keyset, trainset)
    for row in dataset:
        streamdict[row[0]].append(row[1:32])
        #streamdict[row[0]].append(row[1:20])  # selected features
    randomforestmodel = RandomForestClassifier(n_estimators=10, n_jobs=10)

    for k in streamdict:
        #print k
        perTLD = streamdict[k]
        #Full Feature set
        ktarget = np.array([x[0][flimit] for x in perTLD])
        ktrain = np.array([x[0][0:flimit] for x in perTLD])
        randomforestmodel.fit(ktrain, ktarget)
        s = pickle.dumps(randomforestmodel)
        
        models[k].append(s)
       
    #print "[+] Finish"

   
    return models



 
#################################
# The proposed Evlaution method 
################################
def multiLevelEval(l2real, l2predict):
    total = 0
    partial = 0
    unknown = 0
    for i in range(0, len(l2real)):
        temp1 = tldextract.extract(l2real[i])
        temp2 = tldextract.extract(l2predict[i])
        realRD = temp1.domain + "." + temp1.suffix
        predictRD = temp2.domain + "." + temp2.suffix

        if l2real[i] == l2predict[i]:
            total = total + 1.0

        elif realRD == predictRD:
            partial = partial + 1.0
        elif realRD == "google.com" and predictRD == "gstatic.com":
	     total = total + 1.0
	elif predictRD == "google.com" and realRD == "gstatic.com":
           total = total + 1.0
    	else:
            unknown = unknown + 1
        
    print " [++] Partial : " + str(round(float(partial) / len(l2real),2))
    print " [++] Full :" + str(round (float(total) / len(l2real),2))
    print " [++] unknown " + str (round(float(unknown) / len(l2real),2))
    return (float(total) / len(l2real))
#############################################
# Multi-Level Classification 
#############################################
def KFoldFullFeatures(filename,flimit=30):
    dataset = read_csv(filename)
    result = []
    X = np.array([z[0:31] for z in dataset])
    y = np.array([z[31] for z in dataset])

    rf = RandomForestClassifier(n_estimators=50, n_jobs=10)
    kf = cross_validation.StratifiedKFold(y, n_folds=10, indices=None)
    totalac = []
    for train_index, test_index in kf:
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]
        features = np.array([zp[0:flimit] for zp in X_train])
        features_test = np.array([zp[0:flimit] for zp in X_test])
        l2label = np.array([zp[flimit] for zp in X_test])
        Modles = Lv2modelsCreators(X_train, y_train, flimit)

        l1predict = []
        l1real = []
        l2predict = []
        l2real = []

        streamdict = defaultdict(list)
        rf.fit(features, y_train)
        l1 = rf.predict(features_test)
       
        for i in range(0, len(l1)):
            streamdict[l1[i]].append(X_test[i])
        for k in streamdict:
            preTLD = streamdict[k]
            m = pickle.loads(Modles[k][0])
            feature = np.array([x[0:flimit] for x in preTLD])
            labels = np.array([x[flimit] for x in preTLD])
            l2 = m.predict(feature)
            l2predict = l2predict + l2.tolist()
            l2real = l2real + labels.tolist()

        totalac.append(multiLevelEval(l2real, l2predict))

    return np.mean(totalac)
#############################################
# Legacy method Classification 
#############################################
def LegacyMethod(dataset, flimit=41):
    print "[+] Legacy Classification Method , RandomForst Based"
    dataset = read_csv(dataset)

    X = np.array([z[0:29] for z in dataset])
    y = np.array([z[30] for z in dataset])

    rf = RandomForestClassifier(n_estimators=10, n_jobs=10)
    kf = cross_validation.StratifiedKFold(y, n_folds=10, indices=None)
    totalac = []
    l1real = []
    l1predict = []

    for train_index, test_index in kf:
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]

        rf.fit(X_train, y_train)
        l1 = rf.predict(X_test)

        l1real = l1real + y_test.tolist()
        l1predict = l1predict + l1.tolist()
        totalac.append(accuracy_score(l1,y_test))
	print accuracy_score(l1,y_test)
    return np.mean(totalac)


################################
# Classifiaction by range
###################################
def classification(trainset, testset):
    #Create classifaction Model
    #firstmodelsCreators(trainset) #first Level
    #modelsCreators(trainset) #second level

    #Load the first level model
    firstlevelmodel = joblib.load("Models/firstlevel.pkl")

    #Test One by One
    testfile = read_csv(testset)
    #testfile= read_csv("10-tld.csv")
    #modelsnumber=read_csv("models-number.csv")
    #testfile= read_csv("70-tld.csv")

    test_label = [t[43] for t in testfile]
    testfeatures = [t[0:42] for t in testfile]

    subservicelabel = [t[42] for t in testfile]
    testfeatures2 = [t[0:42] for t in testfile]

    #modelsname=[t[0] for t in modelsnumber]
    #modelcounter=[t[1] for t in modelsnumber]

    count = 0
    count2 = 0
    count3 = 0

    first_level_result = []
    second_level_result = []
    l1org = []
    l2org = []

    #First level
    for i in range(0, 10):
        TLD = firstlevelmodel.predict(testfeatures[i])  #Level one
        print str(i) + ":" + TLD[0] + " :-->" + test_label[i]

        #first_level_result.append(TLD[0])
        #l1org.append(test_label[i])


        #if int(modelcounter[modelsname.index(TLD[0])])>=2:
        subservice = SecondLevel(TLD[0], testfeatures[i], subservicelabel[i])  # second level
        second_level_result.append(subservice)
        l2org.append(subservicelabel[i])

        # if TLD[0].find("gstatic.com")==-1 and subservicelabel[i].find("gstatic.com")==-1 :
        first_level_result.append(TLD[0])
        l1org.append(test_label[i])

    print accuracy_score(first_level_result, l1org)
    print accuracy_score(second_level_result, l2org)
    #print zip(first_level_result,l1org)
    #print zip(second_level_result,l2org)
    multiLevelEval(second_level_result, l2org)  # The poposed evaluation


#**************************************
# The importance of features 	    	 
#**************************************
def Features():
    dataset = read_csv("streams/100-tld.csv")
    result = []
    X = np.array([z[0:42] for z in dataset])
    y = np.array([z[42] for z in dataset])

    rf = RandomForestClassifier(n_estimators=10, n_jobs=10)
    kf = cross_validation.StratifiedKFold(y, n_folds=10, indices=None)
    totalac = []
    l1real = []
    l1predict = []
    #print len(kf)
    for train_index, test_index in kf:
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]
        # Build a forest and compute the feature importances
        forest = ExtraTreesClassifier(n_estimators=250, compute_importances=True, random_state=0)
        X = np.array(X_train)
        Y = np.array(y_train)
        forest.fit(X, Y)
        importances = forest.feature_importances_
        indices = np.argsort(importances)[::-1]
        # Print the feature ranking
        print "Feature ranking:"
        for f in xrange(20):
            print "%d. feature %d (%f)" % (f + 1, indices[f], importances[indices[f]])
        # Plot the feature importances of the trees and of the forest
        import pylab as pl

        pl.figure()
        pl.title("Feature importances")

        for tree in forest.estimators_:
            pl.plot(xrange(10), tree.feature_importances_[indices], "r")

        pl.plot(xrange(10), importances[indices], "b")
        pl.show()


#############################################
# First Level Soft voting cases
#############################################
def FirstlevelSoftVoting(flimit):
    dataset = read_csv("streams/100-tld.csv")
    X = np.array([z[0:43] for z in dataset])
    y = np.array([z[43] for z in dataset])

    rf = RandomForestClassifier(n_estimators=20, n_jobs=10)
    kf = cross_validation.StratifiedKFold(y, n_folds=10, indices=None)
    for train_index, test_index in kf:
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]
        features = np.array([zp[0:flimit] for zp in X_train])
        features_test = np.array([zp[0:flimit] for zp in X_test])
        rf.fit(features, y_train)
        #print rf.predict(features_test[0])
        right=[]
        wrong=[]
        #This section to make the softvoting calaculation
        for index in range(0,len(features_test)):
            result= np.zeros((0,68))
            for tree in range(20):
                x=rf.estimators_[tree].predict_proba(features_test[index])
                result=np.append(result,x,axis=0)
            avw=[sum(xl)/20 for xl in zip(*result)]
            ind=np.argmax(avw) #index of max Average weight
            #print rf.classes_[ind] #Predicted value
            #print y_test[0] #Real value
            #print np.amax(avw) #max average weight
            #print "MAX AVR , Predicted, Real"
            label=""
            if rf.classes_[ind]==y_test[index]:
                label="T"
                right.append(round(np.amax(avw),2))
            else:
                label="F"
                wrong.append(round(np.amax(avw),2))
            #print str(np.amax(avw))+", "+label
        print right
        print wrong

       #  bins = np.linspace(0,1,11)
       #  plt.hist(right, bins, alpha=0.5, label='Wright')
       #  plt.hist(wrong, bins, alpha=0.5, label='Wrong')
       #  plt.legend(loc='upper right')
       #  plt.show()
        #print np.histogram(right,bins=12)
        #print np.histogram(wrong,bins=12)
        # plt.hist(right, bins=12)
        # plt.show()
        # plt.hist(wrong, bins=12)
        # plt.show()


            #print([sum(xl)/20 for xl in zip(*result)])
            #print zip([sum(xl)/20 for xl in zip(*result)], rf.classes_)
        break
        #l1=rf.predict(features_test) we sure that the top level over all accuracy is 94.44%
        #print accuracy_score(l1,y_test)

############################################
# This part related to the Real time analysis
###########################################
#   Open url in browser
###########################################
def openurl(filename):
    urls = read_csv(filename)
    for x in range(280, 300):
        webbrowser.open_new_tab("https://www." + urls[x][0])
        time.sleep(10)


##################################################
# Get the graph data from a streams
##################################################
def GetGraphNode(streamfile):
    mincapture = pyshark.FileCapture(streamfile,
                                     display_filter="ssl.handshake.type == 1 and ssl.handshake.extensions_server_name_len >0")

    source = []
    dest = []
    graph = []
    for minpack in mincapture:
        t = minpack.sniff_time.strftime("%s")
        x = int(t) - int(mincapture[0].sniff_time.strftime("%s")) + 1
        clientsni = minpack.ssl.handshake_extensions_server_name
        #print str(x)+  " \""+minpack['ip'].src +"\" -> \""+ clientsni+"\""
        print str(x) + " " + clientsni + " -->" + minpack['ip'].dst

        source.append(minpack['ip'].src)
        dest.append(minpack['ip'].dst)

    for minpack in mincapture:
        t = minpack.sniff_time
        source.append(minpack['ip'].src)
        dest.append(minpack['ip'].dst)



        #for z in range(0,len(dest)):
        #    graph.append((source[z],dest[z]))

        #draw_graph(graph)

        #print graph


################################################
# Draw Network Graph
################################################
def draw_graph(graph):
    # extract nodes from graph
    nodes = set([n1 for n1, n2 in graph] + [n2 for n1, n2 in graph])

    # create networkx graph
    G = nx.DiGraph()


    # add nodes
    for node in nodes:
        G.add_node(node)

    # add edges
    for edge in graph:
        G.add_edge(edge[0], edge[1])

    # draw graph
    pos = nx.shell_layout(G)
    nx.draw(G, pos)

    # show graph
    plt.show()


##########################################
# Jaccard Similarity
#######################################
def jaccard_similarity(x, y):
    intersection_cardinality = len(set.intersection(*[set(x), set(y)]))
    union_cardinality = len(set.union(*[set(x), set(y)]))
    return intersection_cardinality / float(union_cardinality)


##########################################
# Plot graph
##########################################
def plotgraph(p1x, p1y, p2x, p2y, title):
    plt.figure(1)
    plt.subplot(211)
    plt.plot(p1x, p1y)
    plt.ylabel('Number of IP address')
    plt.xlabel("Time")
    plt.title(title)
    plt.subplot(212)
    plt.plot(p2x, p2y)  #23/11/2015
    plt.ylabel('Number of IP address')
    plt.xlabel("Time")
    plt.show()

#######################
#  Soft votinng histogram
############################
def hosto(array):
     uniqelist = np.unique(array)
     print array.count(1)


#############################################
# Soft voting for Multi-Level Classification
#############################################
def SoftVotingML(flimit=42):
    dataset = read_csv("streams/100-tld.csv")
    X = np.array([z[0:43] for z in dataset])
    y = np.array([z[43] for z in dataset])
    rf = RandomForestClassifier(n_estimators=20, n_jobs=10)
    kf = cross_validation.StratifiedKFold(y, n_folds=10, indices=None)

    for train_index, test_index in kf:
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]
        features = np.array([zp[0:flimit] for zp in X_train])
        features_test = np.array([zp[0:flimit] for zp in X_test])
        l2label = np.array([zp[flimit] for zp in X_test])
        Lv2modelsCreators(X_train, y_train, flimit)
        rf.fit(features, y_train)
        right=[]
        wrong=[]
        for index in range(0,5000): #range(len(features_test)):
            l1 = rf.predict(features_test[index]) # levelone prediction
            m = joblib.load("SUB-Models/" + l1[0] + 'RandomForst.pkl')
            #print "had descsion :"+ m.predict(features_test[index])[0]
            xyz=[]
            result=m.estimators_[0].predict_proba(features_test[index])
            for tree in range(1,20):
                x=m.estimators_[tree].predict_proba(features_test[index])
                result=np.append(result,x,axis=0)
            avw=[sum(xl)/20 for xl in zip(*result)]
            ind=np.argmax(avw) #index of max Average weight
            print "Predicted: "+m.classes_[ind] #Predicted value
            print np.amax(avw) #max average weight
            print "Real: "+l2label[index]
            if m.classes_[ind]==l2label[index]:
               right.append(round(np.amax(avw),2))
            else:
               wrong.append(round(np.amax(avw),2))

        print right
        print wrong

        break


#################################################################################
# Realtime functions
################################################################################


# Function for packet size statistics
def PacketSizeCalculation(pktsize):
    result = str(len(pktsize)) + "," + str(math.ceil(numpy.mean(pktsize))) + "," + str(
        math.ceil(numpy.percentile(pktsize, 25))) + "," + str(math.ceil(numpy.percentile(pktsize, 50))) + "," + str(
        math.ceil(numpy.percentile(pktsize, 75))) + "," + str(math.ceil(numpy.var(pktsize))) + "," + str(
        math.ceil(numpy.max(pktsize)))
    # result=[len(pktsize), math.ceil(numpy.mean(pktsize)), math.ceil(numpy.percentile(pktsize, 25)), math.ceil(numpy.percentile(pktsize, 50)),
    #         math.ceil(numpy.percentile(pktsize, 75)), math.ceil(numpy.var(pktsize)), math.ceil(numpy.max(pktsize))]
    return result

# Function for Calculating the Inter Arrival Time and make statistics
def IATCalculcation(arrivaltime):
    counter = 1
    IAT = []

    for item in arrivaltime:
        if (counter != len(arrivaltime)):
            IAT.append((arrivaltime[counter] - arrivaltime[counter - 1]).total_seconds()*1000)  #second
            counter = counter + 1

    result = str(numpy.percentile(IAT, 25)) + "," + str(numpy.percentile(IAT, 50)) + "," + str(numpy.percentile(IAT, 75))
    return result


def IATCalculcation2(arrivaltime):
    counter = 1
    IAT = []
    for item in arrivaltime:
        if (counter != len(arrivaltime)):
            IAT.append((arrivaltime[counter] - arrivaltime[counter - 1])*1000)  #second
            counter = counter + 1
    result = str(numpy.percentile(IAT, 25)) + "," + str(numpy.percentile(IAT, 50)) + "," + str(
        numpy.percentile(IAT, 75))
    del IAT
    return result

# Function used to store statistics
def GetFeatures2(TCS_PSIZE, TCS_TIME, CS_PSIZE, CS_TIME, SC_PSIZE, SC_TIME):
        features= PacketSizeCalculation(TCS_PSIZE) + "," + IATCalculcation2(TCS_TIME) + "," + PacketSizeCalculation(CS_PSIZE) +\
        "," + IATCalculcation2(CS_TIME) + "," + PacketSizeCalculation(SC_PSIZE) + "," + IATCalculcation2(SC_TIME)
        return features

# Function used to store statistics
def GetFeatures(TCS_PSIZE, TCS_TIME, CS_PSIZE, CS_TIME, SC_PSIZE, SC_TIME):
        features= PacketSizeCalculation(TCS_PSIZE) + "," + IATCalculcation(TCS_TIME) + "," + PacketSizeCalculation(CS_PSIZE) +\
        "," + IATCalculcation(CS_TIME) + "," + PacketSizeCalculation(SC_PSIZE) + "," + IATCalculcation(SC_TIME)
        # features = PacketSizeCalculation(TCS_PSIZE) + IATCalculcation(TCS_TIME) + PacketSizeCalculation(CS_PSIZE) + \
        #            IATCalculcation(CS_TIME) +PacketSizeCalculation(SC_PSIZE) + IATCalculcation(SC_TIME)

        return features

##########################################################################
# Realtime  Classifier
#########################################################################
def RealtimeClassifier(filename):
    print "Model Building ..."
    train = read_csv(filename)
    target = np.array([x[30] for x in train])
    train = np.array([x[0:30] for x in train])
    #rf=tree.DecisionTreeClassifier()
    rf = RandomForestClassifier(n_estimators=100, n_jobs=1)

    ac=cross_validation.cross_val_score(rf,train, target, cv=10,n_jobs=10)
    print ac
    output = "flows/Model/model.pkl"
    joblib.dump(rf, output)

def openlink():
    #urls= read_csv("NewDataset/Top514.csv",has_header=False)
    urls =[str(line.strip()) for line in open("target2.txt", "r")]
    for x in range(0,len(urls)):
        cont=webbrowser.get('firefox')
        #cont=webbrowser.get('opera')
        cont.open_new_tab("https://"+urls[x])
        time.sleep(10)
        print x

########################################################################33
#
###########################################################################
