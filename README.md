                 _    _ _______ _______ _____   _____ 
                | |  | |__   __|__   __|  __ \ / ____|
                | |__| |  | |     | |  | |__) | (___  
                |  __  |  | |     | |  |  ___/ \___ \ 
                | |  | |  | |     | |  | |     ____) |
                |_|  |_|  |_|     |_|  |_|    |_____/ 
                
             Early Identification Firewall of HTTPS services

Overview:
This prototype operates by extending the iptables/netfilter architecture. 
It receives and demultiplexes the arriving HTTPS packets to a related flow. 
As soon as the number of packets in a given flow reaches a threshold, 
the identification engine extracts the features and runs the C4.5 algorithm
to predict the HTTPS service of the flow.

Credits :

   This framework was developed by Wazen Shbair, Thibault Cholez, 
   Jérôme François and Isabelle Chrisment of MADYNES research
   group in LORIA, France.                  

Contact:

    Thibault Cholez:  thibault.cholez@loria.fr
    Wazen Shbair:  wazen.shbair@gmail.com
    